Welcome to YouTube Video Uploader

-- Goals --

- Creates a field "YouTube" to upload YouTube videos.
- Provides Direct Video Upload and URL extract formats.
- Creates a YouTube Engine settings form to add YouTube credentials.

@@TODO :

1. Enhance the basic UI for the YouTube form.
2. Provide a better customizable cck field.
3. Add multiple fields functionality.
4. Make field setting load by default, a global setting should be added.
 


-- INSTALLATION --
1. Adding Zend-Google Gdata library :
Firstly, you will need to download the Zend Gdata library from
http://packages.zendframework.com/releases/ZendGdata-1.12.3/ZendGdata-1.12.3.zip
Once downloaded you need to extract the folder in sites/all/libraries
and rename it as ZendGdata. So the complete path will look like :

sites/all/libraries/ZendGdata/library/Zend/Gdata.php


2. Installing and configuring the module
Install as usual, Download the module and paste it in sites/all/modules.
Activate the module in admin/modules.
Navigate to admin/config. 
In the right side block find "YouTube Engine Settings". 
Click on the link. Fill the form. Your google email id and password.

3. To obtain youtube API key,
go to https://console.developers.google.com and create a project.
After you create a project you see APIs & auth on left hand side bar.
Click on APIs & auth>Credentials>create new key> Browser key.
Add you site url with wilcard if (needed) in allowed referers.
Copy the generated API key and add it in YouTube Video Uploader Settings.
(Remember to upload a video using youtube.com if its a new account 
and your gmail is not linked to your youtube account.)

4. Adding the youtube field.
Goto "admin/structure/types" and select manage fields in any content type.
Create a new CCk field. Select Field type as "YouTube".
***Multifield is currently not supported.

-- CONTACT --
Current maintainers:
* Sunny Jhunjhunwala (sunnyuff) - http://drupal.org/user/1787232
