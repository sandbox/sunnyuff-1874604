<?php

/**
 * @file
 * Implements a youtube video uploader field.
 */

/**
 * Implements hook_field_info().
 */
function youtube_video_uploader_field_info() {
  return array(
    'youtube' => array(
      'label' => t('YouTube'),
      'description' => t('Custom youtube field.'),
      'default_widget' => 'youtube_youtube_form',
      'default_formatter' => 'youtube_default',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function youtube_video_uploader_field_widget_info() {
  return array(
    'youtube_youtube_form' => array(
      'label' => t('YouTube Video'),
      'field types' => array('youtube'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function youtube_video_uploader_field_formatter_info() {
  return array(
    'youtube_default' => array(
      'label' => t('Default'),
      'field types' => array('youtube'),
    ),
    'youtube_small' => array(
      'label' => t('Small'),
      'field types' => array('youtube'),
    ),
    'youtube_link' => array(
      'label' => t('Link'),
      'field types' => array('youtube'),
    ),
    'youtube_thumbnail' => array(
      'label' => t('Thumbnail'),
      'field types' => array('youtube'),
    ),
    'youtube_hq' => array(
      'label' => t('HQ Image'),
      'field types' => array('youtube'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function youtube_video_uploader_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'youtube_default':
      foreach ($items as $delta => $item) {
        $element[$delta]['#markup'] = theme('youtube_formatter_default', $item);
      }

      break;

    case 'youtube_small':
      foreach ($items as $delta => $item) {
        $element[$delta]['#markup'] = theme('youtube_formatter_small', $item);
      }

      break;

    case 'youtube_link':
      foreach ($items as $delta => $item) {
        $element[$delta]['#markup'] = theme('youtube_formatter_link', $item);
      }

      break;

    case 'youtube_thumbnail':
      foreach ($items as $delta => $item) {
        $element[$delta]['#markup'] = theme('youtube_formatter_thumbnail', $item);
      }

      break;

    case 'youtube_hq':
      foreach ($items as $delta => $item) {
        $element[$delta]['#markup'] = theme('youtube_formatter_hq', $item);
      }

      break;
  }

  return $element;
}

/**
 * Implements hook_field_is_empty().
 */
function youtube_video_uploader_field_is_empty($item, $field) {
  if ($field['type'] == 'youtube') {

    if (empty($item['Video'])) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Implements hook_field_widget_form().
 */
function youtube_video_uploader_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  if ($instance['widget']['type'] == 'youtube_youtube_form') {
    // Add Ctools support.
    ctools_include('ajax');
    ctools_include('modal');

    // Add CTools' javascript to the page.
    ctools_modal_add_js();
    $element['url'] = array(
      '#type' => 'hidden',
      // The name of the class is the #id of $form['ajax_button'] with "-url"
      // suffix.
      '#attributes' => array('class' => array('youtube-button-url')),
      '#value' => url('youtube/ajax/upload'),
    );
    $element['option_button'] = array(
      '#type' => 'radios',
      '#title' => t('Select your option'),
      '#options' => array(t('YouTube URL'), t('Upload Video')),
      '#default_value' => 0,
      '#attributes' => array('class' => array('youtube_video_uploader_option')),
      '#ajax' => array(
        'callback' => 'youtube_video_uploader_ajax_option',
      ),
    );

    $element['ajax_button'] = array(
      '#type' => 'button',
      '#value' => t('Select Video'),
      '#attributes' => array('class' => array('ctools-use-modal')),
      '#id' => 'youtube-button',
    );
    $element['remove_button'] = array(
      '#type' => 'button',
      '#value' => t('Remove Video'),
      '#id' => 'youtube-button-remove',
      '#ajax' => array(
        'callback' => 'youtube_video_uploader_ajax_remove',
      ),
    );

    $element['Video'] = array(
      '#type' => 'hidden',
      '#title' => t('Video'),
      '#attributes' => array('id' => array('youtube-story-id')),
      '#default_value' => isset($items[$delta]['Video']) ? $items[$delta]['Video'] : NULL,
      '#required' => ($instance['required'] == 1) ? 1 : 0,
    );
    $story_id = isset($items[$delta]['Video']) ? ($items[$delta]['Video']) : '';
    $element['preview'] = array(
      '#type' => 'item',
      '#markup' => isset($items[$delta]['Video']) ? "<img src='http://img.youtube.com/vi/$story_id/default.jpg' class='youtube_hqdefault' />" : '',
      '#prefix' => '<div id="youtube_preview_launcher">',
      '#suffix' => '</div>',
    );
  }
  return $element;
}

/**
 * Function to check user option for url or upload.
 */
function youtube_video_uploader_ajax_option($form, $form_state) {
  youtube_video_uploader_upload_cache_set('option', $form_state['triggering_element']['#value']);
  if (isset($form_state['values']['title'])) {
    youtube_video_uploader_upload_cache_set('node_title', truncate_utf8($form_state['values']['title'], 100, TRUE, TRUE));
  }
  if (isset($form_state['values']['body'])) {
    youtube_video_uploader_upload_cache_set('node_body', drupal_html_to_text(truncate_utf8($form_state['values']['body'][LANGUAGE_NONE]['0']['value'], 120, TRUE, TRUE)));
  }
}

/**
 * Removes the attached video from video field.
 */
function youtube_video_uploader_ajax_remove() {
  $commands[] = ajax_command_invoke("#youtube-story-id", "val", array(''));
  $commands[] = ajax_command_invoke("#youtube_preview_launcher", "html", array(''));
  print ajax_render($commands);
  exit;
}

/**
 * Implements hook_theme().
 */
function youtube_video_uploader_theme($existing, $type, $theme, $path) {
  return array(
    'youtube_formatter_default' => array(
      'variables' => array('item' => NULL),
    ),
    'youtube_formatter_small' => array(
      'variables' => array('item' => NULL),
    ),
    'youtube_formatter_link' => array(
      'variables' => array('item' => NULL),
    ),
    'youtube_formatter_thumbnail' => array(
      'variables' => array('item' => NULL),
    ),
    'youtube_formatter_hq' => array(
      'variables' => array('item' => NULL),
    ),
  );
}

/**
 * Returns Default formatter.
 */
function theme_youtube_formatter_default($item) {
  $story_id = $item['Video'];
  $output = '';
  $output .= '<div class="youtube-wrapper">';
  $output .= "<iframe width='600' height='400' src='http://www.youtube.com/embed/$story_id?HD=1;' frameborder='0' allowfullscreen></iframe>";
  $output .= '</div><br />';
  return $output;
}

/**
 * Returns Small formatter.
 */
function theme_youtube_formatter_small($item) {
  $story_id = $item['Video'];
  $output = '';
  $output .= '<div class="youtube-wrapper">';
  $output .= "<iframe width='200' height='140' src='http://www.youtube.com/embed/$story_id?HD=1;rel=0;showinfo=0;controls=0' frameborder='0' allowfullscreen></iframe>";
  $output .= '</div><br />';
  return $output;
}

/**
 * Returns Link formatter.
 */
function theme_youtube_formatter_link($item) {
  $story_id = $item['Video'];
  $output = '';
  $output .= '<div class="youtube-wrapper">';
  $output .= "<iframe width='100' height='75' src='http://www.youtube.com/embed/$story_id?HD=1;rel=0;showinfo=0;controls=0' frameborder='0' allowfullscreen></iframe>";
  $output .= '</div><br />';
  return $output;
}

/**
 * Returns Link formatter.
 */
function theme_youtube_formatter_thumbnail($item) {
  $story_id = $item['Video'];
  $output = '';
  $output .= '<div class="youtube-wrapper-imagedefault">';
  $output .= "<img src='http://img.youtube.com/vi/$story_id/default.jpg' class='youtube_default' />";
  $output .= '</div><br />';
  return $output;
}

/**
 * Returns Hq Image formatter.
 */
function theme_youtube_formatter_hq($item) {
  $story_id = $item['Video'];
  $output = '';
  $output .= '<div class="youtube-wrapper-imagehq">';
  $output .= "<img src='http://img.youtube.com/vi/$story_id/hqdefault.jpg' class='youtube_hqdefault' />";
  $output .= '</div><br />';
  return $output;
}
