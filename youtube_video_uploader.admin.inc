<?php

/**
 * @file
 * Admin settings handling file for youtube video uploader.
 */

/**
 * Implements system_settings_form (YouTube Engine Settings).
 */
function youtube_video_uploader_engine_settings_form($form, &$form_state) {
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("YouTube Video Uploader Settings"),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['settings']['youtube_video_uploader_engine_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Email id'),
    '#default_value' => (variable_get('youtube_video_uploader_engine_email', '')),
    '#required' => TRUE,
    '#element_validate' => array('youtube_video_uploader_engine_settings_email_element_validate'),
  );
  $form['settings']['youtube_video_uploader_engine_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => (variable_get('youtube_video_uploader_engine_password', '')),
    '#required' => TRUE,
  );
  $form['settings']['youtube_video_uploader_engine_dev_key'] = array(
    '#type' => 'textfield',
    '#title' => t('YouTube Developer key'),
    '#default_value' => (variable_get('youtube_video_uploader_engine_dev_key', '')),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Validation to check if email is in correct format.
 */
function youtube_video_uploader_engine_settings_email_element_validate($element, $form_state) {
  $email_address = $form_state['values']['youtube_video_uploader_engine_email'];
  if (!valid_email_address($email_address)) {
    form_error($element, t('Email, if entered, must be in valid format'));
  }
}
