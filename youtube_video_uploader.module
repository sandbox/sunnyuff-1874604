<?php

/**
 * @file
 * Implements Ajax form for file upload and embedding Youtube URL.
 */

// Include the field element.
require_once dirname(__FILE__) . '/youtube_video_uploader.field.inc';

/**
 * Clearing youtube form cache on node-form on load.
 */
function youtube_video_uploader_form_node_form_alter() {
  youtube_video_uploader_upload_cache_clear('option');
  youtube_video_uploader_upload_cache_clear('node_title');
  youtube_video_uploader_upload_cache_clear('node_body');
}

/**
 * Implements hook_permission().
 *
 * Defines a new permission called 'administer youtube credentials'.
 */
function youtube_video_uploader_permission() {
  return array(
    'administer youtube credentials' => array(
      'title' => t('Administer YouTube Credentials'),
      'description' => t('Permission to add/update Youtube Credentials'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_libraries_info().
 */
function youtube_video_uploader_libraries_info() {
  return array(
    'ZendGdata' => array(
      'name' => 'Zend Gdata',
      'vendor url' => 'http://framework.zend.com/',
      'download url' => 'http://packages.zendframework.com/releases/ZendGdata-1.12.3/ZendGdata-1.12.3.zip',
      'path' => 'library',
      'version arguments' => array(
        'file' => 'README.txt',
        'pattern' => '/Zend Framework (\d[.]\d{1,2}[.]\d)/',
      ),
      'files' => array(
        'php' => array('Zend/Gdata/YouTube.php', 'Zend/Gdata/ClientLogin.php'),
      ),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function youtube_video_uploader_menu() {
  $items = array();
  $items['youtube/%ctools_js/upload'] = array(
    'title' => ('YouTube Uploader'),
    'page callback' => 'youtube_video_uploader_page_callback',
    'page arguments' => array(1),
    'access callback' => '_node_add_access',
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/youtubeengine'] = array(
    'title' => ("YouTube Video Uploader"),
    'description' => "Configure the settings for the Youtube Engine",
    'position' => 'right',
    'weight' => 6,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer youtube credentials'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/youtubeengine/settings'] = array(
    'title' => ('YouTube Video Uploader Settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('youtube_video_uploader_engine_settings_form'),
    'file' => 'youtube_video_uploader.admin.inc',
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer youtube credentials'),
  );
  return $items;
}

/**
 * Page callback for youtube/%ctools_js/upload.
 */
function youtube_video_uploader_page_callback($js = NULL, $step = NULL) {
  if ($js) {
    ctools_include('modal');
    ctools_include('ajax');
  }
  $select_option = youtube_video_uploader_upload_cache_get('option');
  $upload_steps = array(
    'step1' => t('Step 1'),
    'step2' => t('Step 2'),
    'step3' => t('Step 3'));
  $url_steps = array('step1' => t('Step 1'), 'step2' => t('Step 2'));
  $form_upload_steps = array(
    'step1' => array(
      'form id' => 'youtube_video_uploader_step_1',
    ),
    'step2' => array(
      'form id' => 'youtube_video_uploader_step_2',
    ),
    'step3' => array(
      'form id' => 'youtube_video_uploader_step_3',
    ));
  $form_url_steps = array(
    'step1' => array(
      'form id' => 'youtube_video_uploader_url_step_1',
    ),
    'step2' => array(
      'form id' => 'youtube_video_uploader_url_step_2',
    ));
  $form_info = array(
    'id' => 'youtube-upload-form',
    'path' => "youtube/" . ($js ? 'ajax' : 'nojs') . "/upload/%step",
    'show trail' => TRUE,
    'show back' => FALSE,
    'show cancel' => FALSE,
    'show return' => FALSE,
    'next callback' => 'youtube_video_uploader_wizard_next',
    'finish callback' => 'youtube_video_uploader_wizard_finish',
    'cancel callback' => 'youtube_video_uploader_wizard_cancel',
    'order' => ($select_option) ? $upload_steps : $url_steps,
    'forms' => ($select_option) ? $form_upload_steps : $form_url_steps,
  );

  $object_id = 'youtube_upload';

  if (empty($step)) {
    youtube_video_uploader_upload_cache_clear($object_id);
    youtube_video_uploader_upload_cache_clear('story_id');
  }

  $object = youtube_video_uploader_upload_cache_get($object_id);
  $form_state = array(
    'ajax' => $js,
    'object_id' => $object_id,
    'object' => &$object,
  );

  // Send this all off to our form. This is like drupal_get_form only wizardy.
  ctools_include('wizard');
  $form = ctools_wizard_multistep_form($form_info, $step, $form_state);
  $output = drupal_render($form);

  $commands = array();
  if (!empty($form_state['complete'])) {
    $story_id = youtube_video_uploader_upload_cache_get('story_id');
    if ($story_id == 'id') {
      ajax_footer();
    }
    // Dismiss the modal.
    // @TODO : Video id should be unique.
    youtube_video_uploader_upload_cache_clear('option');
    $commands[] = ajax_command_invoke("#youtube-story-id", "val", array($story_id));
    $preview_html = ($story_id && $story_id != 'id') ? "<img src='http://img.youtube.com/vi/$story_id/default.jpg' class='youtube_default' /><div><i>" . t("NOTE : If the image is broken, don't worry. It will appear as soon as youtube finishes processing the video. You can continue with saving the node") . "</i></div>" : '';
    $commands[] = ajax_command_invoke("#youtube_preview_launcher", "html", array($preview_html));
    $commands[] = ctools_modal_command_dismiss();
  }
  elseif (!empty($form_state['cancel'])) {
    // If cancelling, return to the activity.
    $commands[] = ctools_modal_command_dismiss();
  }
  else {
    $commands = ctools_modal_form_render($form_state, $output);
  }
  print ajax_render($commands);
  ajax_footer();
}

/**
 * Implements Form for youtube url step 1.
 */
function youtube_video_uploader_url_step_1($form, &$form_state) {
  $form_state['title'] = t('Youtube Video Upload');
  $form['url'] = array(
    '#title' => t('YouTube URL'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Implements Form for youtube upload step 1.
 */
function youtube_video_uploader_step_1($form, &$form_state) {
  $node_title = youtube_video_uploader_upload_cache_get('node_title');
  $node_body = youtube_video_uploader_upload_cache_get('node_body');
  $form_state['title'] = t('YouTube Video Upload');
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Video Title'),
    '#default_value' => $node_title,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Video Description'),
    '#default_value' => $node_body,
    '#required' => TRUE,
  );
  $form['tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Video Tags'),
    '#required' => TRUE,
  );
  $form['choices'] = array(
    '#type' => 'select',
    '#title' => t('Video Category'),
    '#options' => youtube_video_uploader_options(),
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Returns available youtube categories.
 */
function youtube_video_uploader_options() {
  return array(
    '1' => t('Film'),
    '2' => t('Autos'),
    '3' => t('Music'),
    '4' => t('Animals'),
    '5' => t('Sports'),
    '6' => t('Travel'),
    '7' => t('People'),
    '8' => t('Comedy'),
    '9' => t('Entertainment'),
    '10' => t('News'),
    '11' => t('Howto'),
    '12' => t('Education'),
    '13' => t('Tech'),
    '14' => t('Nonprofit'),
    '15' => t('Movies'),
  );
}

/**
 * Implements Form for youtube url step 2.
 */
function youtube_video_uploader_url_step_2($form, &$form_state) {
  $story_id = youtube_video_uploader_upload_cache_get('story_id');
  $form['status'] = array(
    '#type' => 'item',
    '#title' => t('Upload Status'),
    '#markup' => ((drupal_strlen($story_id) == 11) ? t('Upload Successful | Press Finish Button') : t('Some Error Occurred | Please Try Again')),
    '#prefix' => '<div id="upload_status">',
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Implements Form for youtube video upload step 2.
 */
function youtube_video_uploader_step_2($form, &$form_state) {
  global $base_url;
  // @TODO Change as per the server
  $url = $form_state['object']['post_url'] . "?nexturl=$base_url/youtube/ajax/upload/step3";
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('YouTube Video'),
  );
  $form['token'] = array(
    '#type' => 'hidden',
    '#title' => (''),
    '#default_value' => $form_state['object']['token_value'],
    '#required' => TRUE,
  );
  $form['#action'] = url($url, array('external' => TRUE));
  return $form;
}

/**
 * Implements Form for youtube video upload step 3.
 */
function youtube_video_uploader_step_3($form, &$form_state) {
  // @TODO: $state = youtube_get_video_status($_GET['id']).
  if (isset($_GET['status']) && ($_GET['status']) == 200) {
    $status = t('Upload Successful | Press Finish Button');
    youtube_video_uploader_upload_cache_set('story_id', (isset($_GET['id']) ? $_GET['id'] : "id"));
  }
  else {
    $status = t("Some Error Occurred | Please Try Again");
  }
  $form['status'] = array(
    '#type' => 'item',
    '#title' => t('Upload Status'),
    '#markup' => $status,
    '#prefix' => '<div id="upload_status">',
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Cache helpers set c-tools cache.
 */
function youtube_video_uploader_upload_cache_set($id, $object) {
  ctools_include('object-cache');
  ctools_object_cache_set('youtube_upload_ajax', $id, $object);
}

/**
 * Get the current object from the cache, or default.
 */
function youtube_video_uploader_upload_cache_get($id) {
  ctools_include('object-cache');
  return ctools_object_cache_get('youtube_upload_ajax', $id);
}

/**
 * Clear the wizard cache.
 */
function youtube_video_uploader_upload_cache_clear($id) {
  ctools_include('object-cache');
  ctools_object_cache_clear('youtube_upload_ajax', $id);
}

/**
 * Next callback.
 */
function youtube_video_uploader_wizard_next(&$form_state) {
  $select_option = youtube_video_uploader_upload_cache_get('option');
  if ($select_option) {
    $options = youtube_video_uploader_options();
    $username = variable_get('youtube_video_uploader_engine_email', '');
    $password = variable_get('youtube_video_uploader_engine_password', '');
    $developer_key = variable_get('youtube_video_uploader_engine_dev_key', '');
    if ($username == '' || $password == '' || $developer_key == '') {
      form_set_error('', t('Please Enter your youtube credentials @ admin/config/youtubeengine/settings'));
    }
    // Adding Zend library in the include path because there are some
    // internal require calls which fails with libraries load.
    $path = libraries_get_path('ZendGdata');
    set_include_path(get_include_path() . PATH_SEPARATOR . $path . '/library');
    // Loading the library.
    libraries_load('ZendGdata');
    $authentication_url = 'https://www.google.com/accounts/ClientLogin';
    $http_client = Zend_Gdata_ClientLogin::getHttpClient(
            $username, $password, 'youtube', NULL, 'DrupalYoutubeVideoUploader', NULL, NULL, $authentication_url);
    $youtube_client = new Zend_Gdata_YouTube($http_client, NULL, NULL, $developer_key);
    $youtube_client->setMajorProtocolVersion(2);
    $my_video_entry = new Zend_Gdata_YouTube_VideoEntry();
    $my_video_entry->setVideoTitle($form_state['values']['title']);
    $my_video_entry->setVideoDescription($form_state['values']['description']);
    $my_video_entry->setVideoCategory($options[$form_state['values']['choices']]);
    $my_video_entry->SetVideoTags($form_state['values']['tags']);
    $token_handler_url = 'http://gdata.youtube.com/action/GetUploadToken';
    $token_array = $youtube_client->getFormUploadToken($my_video_entry, $token_handler_url);
    $token_value = $token_array['token'];
    $post_url = $token_array['url'];
    $form_state['values']['post_url'] = $post_url;
    $form_state['values']['token_value'] = $token_value;
    $form_state['object'] = $form_state['values'];
    youtube_video_uploader_upload_cache_set($form_state['object_id'], $form_state['object']);
  }
  else {
    $story_id = youtube_video_uploader_get_story_id($form_state['values']['url']);
    youtube_video_uploader_upload_cache_set('story_id', $story_id);
  }
}

/**
 * Returns youtube story id from youtube url.
 */
function youtube_video_uploader_get_story_id($string) {
  $match = '';
  preg_match('/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/', $string, $match);
  if ($match && strlen(check_plain($match[2])) == 11) {
    return $match[2];
  }
  else {
    return NULL;
  }
}

/**
 * Ctools form finish handler.
 */
function youtube_video_uploader_wizard_finish(&$form_state) {
  $form_state['complete'] = TRUE;
}

/**
 * Ctools form Cancel handler.
 */
function youtube_video_uploader_wizard_cancel(&$form_state) {
  $form_state['cancel'] = TRUE;
}
